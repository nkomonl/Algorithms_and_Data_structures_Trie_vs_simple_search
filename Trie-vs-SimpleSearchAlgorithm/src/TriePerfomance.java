import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TriePerfomance {

	private static ArrayList<String> wordList = new ArrayList<String>();
	private static int sizeN;
	private static Scanner sc2;
	private static Scanner sc3;
	private static String wordToMatch;
	private static int wordsToLeft;
	private static int wordsToRight;
	private static boolean firstOccurance = false;
	private static int numberOfwordb4firstOccurance;
	private static boolean wordFound = false;

	static TrieNode createTree() {
		return (new TrieNode('\0'));
	}

	static void insertWord(TrieNode root, String word) {

		int offset = 97;
		int l = word.length();
		char[] letters = word.toCharArray();
		TrieNode curNode = root;

		for (int i = 0; i < l; i++) {
			if (curNode.links[letters[i] - offset] == null)
				curNode.links[letters[i] - offset] = new TrieNode(letters[i]);
			curNode = curNode.links[letters[i] - offset];
		}
		curNode.fullWord = true;
	}

	static boolean find(TrieNode root, String word) {
		char[] letters = word.toCharArray();
		int l = letters.length;
		int offset = 97;
		TrieNode curNode = root;

		int i;
		for (i = 0; i < l; i++) {
			if (curNode == null)
				return false;
			curNode = curNode.links[letters[i] - offset];
		}

		if (i == l && curNode == null)
			return false;

		if (curNode != null && !curNode.fullWord)
			return false;

		return true;
	}

	static void printTree(TrieNode root, int level, char[] branch) {
		if (root == null)
			return;

		for (int i = 0; i < root.links.length; i++) {
			branch[level] = root.letter;
			printTree(root.links[i], level + 1, branch);
		}

		if (root.fullWord) {
			for (int j = 1; j <= level; j++)
				System.out.print(branch[j]);
			System.out.println();
		}
	}

	public static void main(String[] args) {

		int totalcount = 0;

		readTheTextfile();
		input();
		// send arraylist to array
		long elapsedTimeAll = 0;
		// Start the 50 times run of searching the choosen textfile
		for (int ftr = 0; ftr < 500; ftr++) {
			final long startTime = System.nanoTime();
			final long elapsedTime;
			String[] words = wordList.toArray(new String[wordList.size()]);

			// TRIE AND HASH MAP START HERE
			TrieNode tree = createTree();
			HashMap<Integer, String> hmap = new HashMap<Integer, String>();
			// String[] words = {"an", "the","ant", "the","all", "allot",
			// "alloy","the","bus","ride","is","here" };
			int i;
			for (i = 0; i < words.length; i++) {
				insertWord(tree, words[i]);
				hmap.put(i, words[i]);
			}

			char[] branch = new char[50];
			printTree(tree, 0, branch);

			String searchWord = wordToMatch.trim();

			int magicNum = numberOfwordb4firstOccurance;

			if (find(tree, searchWord)) {
				elapsedTime = System.nanoTime() - startTime;
				System.out.println("The word was found");
				// if word is present..now use the string array to search for
				// the extra words
				// ie to get two words before or after the word

				int t;

				for (Map.Entry<Integer, String> entry : hmap.entrySet()) {
					int key = entry.getKey();
					String value = entry.getValue();

					if (value.equals(searchWord)) {

						System.out.println(key + ": " + value);
						for (t = 0; t < magicNum; t++) {
							int mainKey = key + t + 1;

							int nagativekey = key - t - 1;
							System.out.println("index to print are[" + mainKey
									+ " : " + nagativekey + " ].");
							if ((mainKey > 0) && (nagativekey > 0)) {
								System.out.println("the words are =>["
										+ words[mainKey] + " : "
										+ words[nagativekey] + " ].");
							}

						}

					}
				}

				// use this to test performance inclusive hash map effect by
				// setting elapsed time below rather than above
				// elapsedTime = System.nanoTime() - startTime;

			} else {
				elapsedTime = System.nanoTime() - startTime;
				System.out.println("The word was NOT found");
			}
			totalcount++;
			elapsedTimeAll = elapsedTimeAll + elapsedTime;
			long averagetime = 0;

			if (ftr != 0) {
				averagetime = elapsedTimeAll / Long.valueOf(ftr);
			}
			System.out
					.println("==============================================================="
							+ "====\n-----------------------------------------\n------total count is at:.. "
							+ totalcount
							+ "\n====>the average time in nanotime is "
							+ averagetime);

		}

	}

	private static void readTheTextfile() {
		BufferedReader br = null;
		try {
			String textData;
			br = new BufferedReader(new FileReader("textfiles/2000.txt"));

			while ((textData = br.readLine()) != null) {
				// System.out.println(textData);

				String[] words1 = textData.trim().split(
						"[[ ]*|[,]*|[\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*]+");
				for (int i = 0; i < words1.length; i++) {
					if (!words1[i].isEmpty()) {
						wordList.add(words1[i]);
						System.out.println(words1[i]);
					}
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void input() {
		// System.out.println("\n......." + wordList);

		wordToMatch = "";
		sc2 = new Scanner(System.in);
		System.out.print("\n\nplease enter the word to find from the text.. ");
		wordToMatch = sc2.nextLine();

		sc3 = new Scanner(System.in);

		System.out.print("\n\nNow enter the size of N  before and after.. ");
		sizeN = sc3.nextInt();
		numberOfwordb4firstOccurance = sizeN;

	}

}

class TrieNode {
	char letter;
	TrieNode[] links;
	boolean fullWord;

	TrieNode(char letter) {
		this.letter = letter;
		links = new TrieNode[26];
		this.fullWord = false;
	}
}