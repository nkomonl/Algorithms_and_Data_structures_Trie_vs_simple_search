import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class SimpleAlgorithm {

	private static ArrayList<String> wordList = new ArrayList<String>();
	private static int sizeN;
	private static Scanner sc2;
	private static Scanner sc3;
	private static String wordToMatch;
	private static int wordsToLeft;
	private static int wordsToRight;
	private static boolean firstOccurance = false;
	private static int numberOfwordb4firstOccurance;
	private static boolean wordFound = false;

	public static void main(String[] args) {
		// this here reads the textfile and puts the line to a string,
		// then split that new string to an array, then lastly add the
		// individual arrays to an arraylist
		readTheTextfile();
		// now all words are in an arraylist
		// lets take input
		input();
		// now we analyse the arraylist and match to the word we are searching
		final long startTime = System.nanoTime();
		getWord();
		final long elapsedTime = System.nanoTime() - startTime;
		System.out.print("\nTime taken by size " + wordList.size() + "is .."
				+ elapsedTime + "Nanoseconds ");

	}

	private static void readTheTextfile() {
		BufferedReader br = null;
		try {
			String textData;
			br = new BufferedReader(new FileReader("textfiles/2000.txt"));

			while ((textData = br.readLine()) != null) {
				// System.out.println(textData);

				String[] words = textData
						.split("[[ ]*|[,]*|[\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*]+");
				for (int i = 0; i < words.length; i++) {
					wordList.add(words[i]);
					// System.out.println(words[i]);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void input() {
		// System.out.println("\n......." + wordList);

		wordToMatch = "";
		sc2 = new Scanner(System.in);
		System.out.print("\n\nplease enter the word to find from the text.. ");
		wordToMatch = sc2.nextLine();
		sc3 = new Scanner(System.in);

		// wordToMatch="nkomonl";

		// now we process the input and make the snippert

		//
		// if (input word =
		// match){System.out.prinln("now enter the size of N before and after");}else
		// if(word it not match){system...()};
		//
		//

		System.out.print("\n\nNow enter the size of N  before and after.. ");
		sizeN = sc3.nextInt();
		// sizeN=5;
		numberOfwordb4firstOccurance = sizeN;
	}

	private static void getWord() {
		// TODO Auto-generated method stub
		for (int r = 0; r < wordList.size(); r++) {

			// System.out.print("kkkkkkkkkkkkkkkkkkkk");
			if (wordToMatch.equals(wordList.get(r))) {
				wordsToLeft = sizeN;
				// r is index of arraylist<string>
				// so to correct for start bound

				if ((firstOccurance == true) && (wordList.size() >= r + sizeN)) {
					// *******************************************
					System.out
							.println("\n\n///////////////////////\n**Words before the given SearchWord are -- ");
					if (sizeN == r || sizeN < r)

					{
						for (int w = 0; w < sizeN; w++) {
							System.out.print(": "
									+ wordList.get(r - wordsToLeft));
							wordsToLeft--;
						}
					}
					System.out
							.println("\n**--Found SearchWord in***Arraylist wordlist  at Index .."
									+ r
									+ "****\n**Search is for the SearchWord***"
									+ wordToMatch
									+ "*****\n**The words that follow are below*** ");

					for (wordsToRight = 0; wordsToRight < sizeN; wordsToRight++) {
						System.out.print(": "
								+ wordList.get(r + wordsToRight + 1));

					}
				}
				if ((firstOccurance == false) && (wordList.size() >= r + sizeN)) { // ***********************

					if (numberOfwordb4firstOccurance > r) {
						numberOfwordb4firstOccurance--;

						System.out
								.print("\n****Making adjustments for first word.\n");
						getWord();
						return;
					}
					System.out
							.println("\n\n///////////////////////\n**Words before the given SearchWord are -- ");
					if (numberOfwordb4firstOccurance <= r) {
						int tt = numberOfwordb4firstOccurance;
						firstOccurance = true;
						for (int ww = 0; ww < numberOfwordb4firstOccurance; ww++) {

							System.out.print(": " + wordList.get(r - tt));
							tt--;

						}
					}

					System.out
							.println("\n**Found SearchWord in***Arraylist wordlist  at Index .."
									+ r
									+ "****\n**Search is for the SearchWord***"
									+ wordToMatch
									+ "*****\n**The words that follow are below*** ");

					for (wordsToRight = 0; wordsToRight < sizeN; wordsToRight++) {
						System.out.print(": "
								+ wordList.get(r + wordsToRight + 1));

					}

				}

				if (wordList.size() < r + sizeN) { // ***********************
					System.out
							.print("\n\n...Ending Word is less than [ N ] the check....\n");
					int sizeN2 = sizeN;
					for (int ww3 = 0; ww3 < sizeN; ww3++) {
						// System.out.print(" mmmmmmmmmmmmmmmmmmmmmm");

						System.out.print(": " + wordList.get(r - sizeN2));
						sizeN2--;

					}

					System.out
							.println("\n**-Found SearchWord in***Arraylist wordlist  at Index .."
									+ r
									+ "****\n**Search is for the SearchWord***"
									+ wordToMatch
									+ "*****\n**The words that follow are below*** ");

					for (wordsToRight = 1; wordsToRight < wordList.size() - r; wordsToRight++) {
						System.out.print(": " + wordList.get(r + wordsToRight));

					}

				}

			}

			// ******************************
			// else {
			// System.out.println("This word is not in the textfile.");
			// return;
			// }

		}
	}
}
